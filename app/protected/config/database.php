<?php

return array(
	'connectionString' => 'mysql:host=localhost;port:33060;dbname=book_yii',
	'emulatePrepare' => true,
	'username' => 'homestead',
	'password' => 'secret',
	'charset' => 'utf8',
);